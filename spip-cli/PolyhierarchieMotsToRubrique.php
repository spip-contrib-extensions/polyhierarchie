<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;

class PolyhierarchieMotsToRubrique extends Command {
	protected function configure() {
		$this
			->setName('polyhierarchie:mots_to_rubrique')
			->setDescription('Associer en polyhierarchie a une rubrique les contenus déjà associés à un ou plusieurs mots-clés.')
			->addOption(
				'id_rubrique',
				'',
				InputOption::VALUE_OPTIONAL,
				'Identifiant de la rubrique dans laquelle on veut verser les contenus',
				0
			)
			->addOption(
				'id_mot',
				'',
				InputOption::VALUE_OPTIONAL,
				'ID du ou des mots (séparés par une virgule) sur la base duquel on veut selectionner les contenus',
				''
			)
			->addOption(
				'quoi',
				'',
				InputOption::VALUE_OPTIONAL,
				'objet ou liste d\'objets que l\'on veut associer (par défaut tous)',
				''
			)
			->addOption(
				'dry-run',
				'',
				InputOption::VALUE_NONE,
				'Pour voir les contenus qui seront trouvés et ce qui serait fait, mais aucune modification en base',
				null
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('action/editer_objet');
		include_spip('inc/autoriser');
		include_spip('inc/polyhier');


		// Récupérer les options
		$id_rubrique = $input->getOption('id_rubrique');
		if (!$id_rubrique = intval($id_rubrique)) {
			$this->io->error('Indiquez une rubrique');
			return self::FAILURE;
		} elseif (!$rubrique = sql_fetsel('id_rubrique,titre', 'spip_rubriques', 'id_rubrique=' . intval($id_rubrique))) {
			$this->io->error("La rubrique $id_rubrique n'existe pas");
			return self::FAILURE;
		}

		$id_mots = $input->getOption('id_mot');
		$id_mots = explode(',', $id_mots);
		$id_mots = array_map('intval', $id_mots);
		$id_mots = array_filter($id_mots);
		if (empty($id_mots)) {
			$this->io->error('Indiquez un ou plusieurs mots clés');
			return self::FAILURE;
		}

		$dry_run = $input->getOption('dry-run');

		$this->io->title("Ajouter des contenus dans la rubrique #$id_rubrique " . $rubrique['titre']);
		if ($dry_run) {
			$this->io->care('SIMULATION');
		}

		$quoi = $input->getOption('quoi');
		if ($quoi) {
			$quoi = explode(',', $quoi);
			$quoi = array_map('trim', $quoi);
			$quoi = array_filter($quoi);
		}


		// On va chercher tous les mots demandés
		if ($mots = sql_allfetsel('id_mot, titre', 'spip_mots', sql_in('id_mot', $id_mots))) {
			$nb_added = 0;
			foreach ($mots as $mot) {
				$id_mot = intval($mot['id_mot']);
				$this->io->section("MOT #$id_mot " . $mot['titre']);

				// On va chercher tous les liens de ce mot
				$where_quoi = '';
				if (!empty($quoi)) {
					$where_quoi = ' AND ' . sql_in('objet', $quoi);
				}
				if ($liens = sql_allfetsel('*', 'spip_mots_liens', 'id_mot = ' . intval($id_mot) . $where_quoi)) {
					$this->io->text(count($liens) . ' contenus');
					$added = [];
					$deja = [];
					foreach ($liens as $lien) {
						$row = sql_fetsel('*', table_objet_sql($lien['objet']), id_table_objet($lien['objet']) . '=' . intval($lien['id_objet']));
						if (
							(empty($row['id_rubrique']) or $row['id_rubrique'] != $id_rubrique)
							and !sql_countsel('spip_rubriques_liens', ['id_parent=' . intval($id_rubrique), 'objet=' . sql_quote($lien['objet']), 'id_objet=' . intval($lien['id_objet'])])
						) {
							if (empty($added[$lien['objet']])) {
								$added[$lien['objet']] = [];
							}
							$added[$lien['objet']][] = $lien['id_objet'];
							if (!$dry_run) {
								sql_insertq('spip_rubriques_liens', ['id_parent' => $id_rubrique, 'objet' => $lien['objet'], 'id_objet' => $lien['id_objet']]);
							}
						}
						else {
							if (empty($deja[$lien['objet']])) {
								$deja[$lien['objet']] = [];
							}
							$deja[$lien['objet']][] = $lien['id_objet'];
						}
					}

					$objets = array_merge(array_keys($added), array_keys($deja));
					$objets = array_unique($objets);
					sort($objets);
					foreach ($objets as $objet) {
						if (!empty($deja[$objet])) {
							$this->io->care(count($deja[$objet]) . " $objet déjà dans la rubrique : " . implode(',', $deja[$objet]));
						}
						if (!empty($added[$objet])) {
							$nb_added += count($added[$objet]);
							$this->io->check(count($added[$objet]) . " $objet ajoutés : " . implode(',', $added[$objet]));
						}
					}
				}
			}

			// On publie les rubriques qui ont des liens
			polyhier_calculer_rubriques_if($id_rubrique, ['statut' => 'publie', 'add' => [], 'remove' => []]);

			if (!$dry_run) {
				if ($nb_added) {
					$this->io->success("$nb_added contenus ajoutés dans la rubrique #$id_rubrique");
				}
				else {
					$this->io->text("\n\n");
					$this->io->care('Rien à faire');
				}
			}
			return self::SUCCESS;
		} else {
			$this->io->error('Aucun mot trouvé dans la liste fournie');
			return self::FAILURE;
		}
	}
}
