<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-polyhier?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'polyhier_description' => 'セクションや記事を直接の親以外のセクションに付けることで、クロスリンクを作成することができるプラグインです。',
	'polyhier_slogan' => '複数の親のセクションに記事や他のセクションを付けること。'
);
