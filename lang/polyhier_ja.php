<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/polyhier?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'articles_associes' => '関連記事',

	// L
	'label_autre_parent' => '他のセクションにも',
	'label_autres_parents' => '他のセクションにも',
	'label_parents' => 'セクションの中に',
	'label_profondeur_chemin' => 'プライベートエリアのパスの深さ',
	'label_profondeur_chemin_0' => '全パス',
	'label_profondeur_chemin_1' => '上の間接のセクションだけ',
	'label_profondeur_chemin_2' => '2つのレベルのセクション',
	'label_profondeur_chemin_3' => '3つのレベルのセクション',

	// P
	'parent_obligatoire' => '少なくともセクションを一つ選んでください。',

	// R
	'rubriques_associees' => '関連セクション',

	// T
	'titre_polyhierarchie' => '多階層'
);
